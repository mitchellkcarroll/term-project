import pyb
from encoderDriver import EncoderDriver
from motorDriver import MotorDriver





if __name__ == '__main__':
    
    # Init Motor
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15)
    pin_nFAULT = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN) #cpu.B2, pyb.Pin.IN)
    
    
    tim3 = pyb.Timer(3, freq=20000)
    
    M1_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B4),
              'Channel': 1 }
    M1_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B5),
              'Channel': 2 }
    moe1 = MotorDriver(pin_nSLEEP, pin_nFAULT, M1_CH1, M1_CH2, tim3, debug=False, IRQ=False)
    
    
    M2_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B0),
              'Channel': 3 }
    M2_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B1),
              'Channel': 4 }
    moe2 = MotorDriver(pin_nSLEEP, pin_nFAULT, M2_CH1, M2_CH2, tim3, debug=False)
    
    # # Init Encoder
    # E1_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B6),
    #           'Channel': 1 }
    # E1_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B7),
    #           'Channel': 2 }
    # tim4 = pyb.Timer(4, prescaler=0, period=0xFFFF)
    # enc1 = EncoderDriver(E1_CH1, E1_CH2, tim4, debug=True)
    
    # E2_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.C6),
    #           'Channel': 1 }
    # E2_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.C7),
    #           'Channel': 2 }
    # tim8 = pyb.Timer(8, prescaler=0, period=0xFFFF)
    # enc2 = EncoderDriver(E2_CH1, E2_CH2, tim8, debug=True)

    moe1.enable()
    
    while True:
        try:
            # val = int(input("Set motor duty: ")) # Prompt user input
            moe1.set_duty(35)
            moe2.set_duty(35)
            pyb.delay(500)
        except:
            moe1.set_duty(0)
            moe2.set_duty(0)
            print("Exiting")
            break
        
    
    # interval = 100
    
    # start_time = pyb.millis()
    # next_time = interval + pyb.elapsed_millis(start_time) 

    # while True:
    #     if (pyb.elapsed_millis(start_time) >= next_time):
    #         # task.update()
            
    #         enc1.update()
    #         enc2.update()
    #         # enc2.update()
            
            
            
            
    #         # enc2_position = enc2.get_position()
    #         # # W_meas = enc.get_delta() * 4 * 10**3 * 60 / (interval * 1000 * 4) # Calculate motor speed (rpm)                                     # Update speed array
    #         # print("Pos_1 = " + str(enc1_position))
    #         # print("Pos_2 = " + str(enc2_position))
        
    #         next_time += interval # Update the "Scheduled" timestamp
        
        
