'''
@file main.py

@brief The main script file that is used to run the control system of a 2DoF ball balancing system.

@details This file contains the initialization of system objects (motors, encoders, RTP, IMU, feedback controllers) that
         are then controlled via a FSM task manager. This file should be executed to run the system control.
         
@package Lab0x09

@brief This package contains main.py, taskController.py, closedLoop.py, motorDriver.py, encoderDriver.py, bno055.py, bno055_base.py, and RTP_Driver.py
         
@author Grant Gallagher
@author Mitchell Carroll

@date March 17, 2021
'''
# %% Import Libraries

import pyb
import machine

from encoderDriver import EncoderDriver
from motorDriver import MotorDriver
from RTP_Driver import RTP_Driver
from bno055 import BNO055
from closedLoop import ClosedLoop
from taskController import TaskController

# %% Initialize motor objects
print('Initializing motors..')

## Motor nSLEEP pin object (used to enable the driver chip), initialized to PA5
pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
## Motor nFAULT pin object (used for fault detection), initialized to the blue user button.
pin_nFAULT = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN)
## A timer object using timer 3 with a frequency of 20kHz.
tim3 = pyb.Timer(3, freq=20000)


## A dictionary item containing the pin address and channel number for the M1- output pin.
M1_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B4),
          'Channel': 1 }
## A dictionary item containing the pin address and channel number for the M1+ output pin.
M1_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B5),
          'Channel': 2 }
## A MotorDriver object used to control the output functions of Motor 1.
moe1 = MotorDriver(pin_nSLEEP, pin_nFAULT, M1_CH1, M1_CH2, tim3)


## A dictionary item containing the pin address and channel number for the M2- output pin.
M2_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B0),
          'Channel': 3 }
## A dictionary item containing the pin address and channel number for the M2+ output pin.
M2_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B1),
          'Channel': 4 }
## A MotorDriver object used to control the output functions of Motor 2.
moe2 = MotorDriver(pin_nSLEEP, pin_nFAULT, M2_CH1, M2_CH2, tim3)

# Enable BOTH motors.
moe1.enable()
moe2.enable()

# %% Initialize encoder objects
print('Initializing encoders..')
## A dictionary item containing the pin address and channel number for the E1_CH1 input pin.
E1_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B6),
          'Channel': 1 }
## A dictionary item containing the pin address and channel number for the E1_CH2 input pin.
E1_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B7),
          'Channel': 2 }
## A timer object using timer 4 with a period of 65535.
tim4 = pyb.Timer(4, prescaler=0, period=0xFFFF)
## An EncoderDriver object used to measure the count and position of Encoder 1.
enc1 = EncoderDriver(E1_CH1, E1_CH2, tim4, debug=False)


## A dictionary item containing the pin address and channel number for the E2_CH1 input pin.
E2_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.C6),
          'Channel': 1 }
## A dictionary item containing the pin address and channel number for the E2_CH2 input pin.
E2_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.C7),
          'Channel': 2 }
## A timer object using timer 8 with a period of 65535.
tim8 = pyb.Timer(8, prescaler=0, period=0xFFFF)
## An EncoderDriver object used to measure the count and position of Encoder 1.
enc2 = EncoderDriver(E2_CH1, E2_CH2, tim8, debug=False)

# %% Initialize the resistive touch panel object
print('Initializing touch panel..')
## The Nucleo board pin name associated with the x_p lead on the resistive touch panel.
PA0 = pyb.Pin.board.PA0
## The Nucleo board pin name associated with the x_m lead on the resistive touch panel.
PA1 = pyb.Pin.board.PA1
## The Nucleo board pin name associated with the y_p lead on the resistive touch panel.
PA6 = pyb.Pin.board.PA6
## The Nucleo board pin name associated with the y_m lead on the resistive touch panel.
PA7 = pyb.Pin.board.PA7

## The calibration distance [mm] between two points along the horizontal axis, respective to x_count_cal.
x_len_cal        = (-20,  20)
## The calibration count between two points along the horizontal axis, respective to x_len_cal.
x_count_cal      = (1636, 2452)
## The calibration distance [mm] between two points along the vertical axis, respective to y_count_cal.
y_len_cal        = (-20,  20)
## y_count_cal  The calibration count between two points along the vertical axis, respective to y_len_cal.
y_count_cal      = (1291, 2585)
## The calibration count, located at the center of the platform along the RTP. (x_count, y_count).
center_count_cal = (2046, 2035) 

## An RTP_Driver object used to measure the contact point on the resistive touch panel.
rtp = RTP_Driver(PA0, PA1, PA6, PA7, x_len_cal, x_count_cal, y_len_cal, y_count_cal, center_count_cal)

# %% Initialize IMU object
print('Initializing IMU..')

## Pyboard hardware I2C.
i2c = machine.I2C(1)

## An IMU object used to measure the orientation of the platform during calibration.
imu = BNO055(i2c)

# %% Controller Attributes
print('Initializing State Feedback Controller..')

## A float representing the control system gain applied to the ball velocity [N*s].
k1 = -40

## A float representing the control system gain applied to the platform velocity [N*m*s].
k2 = -0.8

## A float representing the control system gain applied to the ball position [N].
k3 = -100

## A float representing the control system gain applied to the platform position [N*m].
k4 = -12

## A float representing the motor gain value.
k_m = 2.21*1000/(12*13.8)

## A 1x4 array (list) containing the control systems gains.
k_ctrl = [k1, k2, k3, k4]

## A ClosedLoop controller object used to calculate the PWM level of Motor 1 with full-state feedback.
controller1 = ClosedLoop(k_m, k_ctrl)

## A ClosedLoop controller object used to calculate the PWM level of Motor 2 with full-state feedback.
controller2 = ClosedLoop(k_m, k_ctrl)

# %%
## An integer representing the time, in ms, between iterations of the TaskController FSM.
interval = 12
## A TaskController object that is used to control the measurements and actuation of the system.
system = TaskController(moe1, moe2, enc1, enc2, controller1, controller2, rtp, imu, interval, debug=False)
 


if __name__ == '__main__':   
    # Run the system continuously until:
    # 1) The user pressed CTRL+C
    # 2) The motor detects a fault
    # 3) The power supply is turned off.
    system.run()