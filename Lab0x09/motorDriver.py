'''
@file  motorDriver.py

@brief  This file contains a motor driver class.

@details  This file contains a single motor driver class that is used to drive a motor
          from the Nucleo L476RG board. The driver implements bidirectional, PWM control
          of at a user-specified frequency. The motor can be set to one of four various states
          found in the truth table of the DRV8847 chip: Sleep (disabled), Coast (enabled && duty = 0),
          Reverse Direction (duty < 0), or Forward Direction (duty > 0).
          
@package Lab0x09

@author Grant Gallagher
@author Mitchell Carroll

@date March 17, 2021
'''
import pyb
import micropython

## An emergency buffer to store errors that are thrown inside ISR.
micropython.alloc_emergency_exception_buf(200)
        
class MotorDriver:
    '''
    @brief   This class implements a motor driver for the ME405 board.
    
    @details This motor driver class implements bidirectional, PWM control of
             a DCX-22S Brushed DC motor found on the ME405 breakout board. The
             motor can be enabled, disabled, or set to a PWM duty cycle between
             -100 and 100 (for each direction).
    '''
    def __init__(self, pin_nSLEEP, pin_nFAULT, IN1, IN2, timer, debug=False, IRQ=False):
        '''
        @brief Creates a motor driver object by initializing GPIO pins and turning the motor off for safety.
        @param pin_nSLEEP  A pyb.Pin object correlatied with the sleep pin on the DRV8847 motor driver chip.
        @param pin_nFAULT  A pyb.Pin object correlatied with the fault detection pin on the DRV8847 motor driver chip.
        @param IN1 A pyb.Pin object to use as the input to half bridge 1.
        @param IN2 A pyb.Pin object to use as the input to half bridge 2.
        @param timer   A pyb.Timer object to use for PWM generation on pin_IN1 and pin_IN2.
        @param debug   A boolean value that determines debugging state.
        @param IRQ     A boolean value that determines if an interrupt service initialized for fault detection.
                       NOTE: only one MotorDriver object can utilize an ISR, since they share the same chip.
        '''
        ## A pyb.Pin object correlatied with the sleep pin on the DRV8847 motor driver chip.
        self.pin_nSLEEP = pin_nSLEEP
        self.pin_nSLEEP.low() # Disable upon startup to avoid damage
        
        ## A pyb.Pin object correlatied with the fault detection pin on the DRV8847 motor driver chip - active low.
        self.pin_nFAULT = pin_nFAULT
                
        ## The timer channel used to control pin_IN1 with PWM.
        self.timer_ch1 = timer.channel(IN1['Channel'], pyb.Timer.PWM, pin=IN1['Pin'])
        
        ## The timer channel used to control pin_IN2 with PWM.
        self.timer_ch2 = timer.channel(IN2['Channel'], pyb.Timer.PWM, pin=IN2['Pin'])
        
        ## A boolean representing whether a fault is currently detected and/or cleared.
        self.fault = False
        
        if IRQ == True:
            ## The external interrupt object for when the nFAULT pin goes low.
            self.extint = pyb.ExtInt(self.pin_nFAULT, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_NONE, self.callback)

        ## The debug state (bool).
        self.debug = debug
        
        if self.debug:
            print('Creating a MotorDriver object')    
            
    def enable(self):
        '''
        @brief  Enables the motor.
        '''
        self.pin_nSLEEP.high() # Raise the sleep mode pin
        
        if self.debug: # When debug == true
            print('Enabling Motor')
    
    
    def disable(self):
        '''
        @brief  Disables the motor and sets the PWM duty cycle to 0.
        '''
        self.pin_nSLEEP.low() # Lower the sleep mode pin
        self.set_duty(0)      # Turn off signals for redundancy
        
        if self.debug:
            print('Disabling Motor')
            
    
    def set_duty(self, duty):       
        '''
        @brief Sets the duty cycle to be sent to the motor.
        @details Positive values cause effort in one direction, negative values in the opposite direction.
        @param duty  A signed integer holding the duty cycle of the PWM signal sent to the motor
        '''        
        if self.fault == True:
            self.check_fault()
        
        elif (abs(duty) > 100):
            if duty < 0:
                duty = -100
                
            elif duty > 0:
                duty = 100
                
            if self.debug: # When debug == true
                print('Error. duty must be an integer between -100 and 100')
            pass
        
        elif duty == 0: # Motor Coast
            self.timer_ch1.pulse_width_percent(0) # Set duty cycle to 0
            self.timer_ch2.pulse_width_percent(0) # Set duty cycle to 0
            
        elif duty > 0: # Motor Forward
            self.timer_ch1.pulse_width_percent(duty)    # Set duty cycle to 0
            self.timer_ch2.pulse_width_percent(0) # Set duty cycle to value of input argument
        
        elif duty < 0: # Motor Reverse 
            self.timer_ch1.pulse_width_percent(0) # Set duty cycle to absolute value of input argument
            self.timer_ch2.pulse_width_percent(abs(duty))         # Set duty cycle to 0
        
        else:
            pass
            
        if self.debug and self.fault == False: # When debug == true
            print('Duty cycle set to: ' + str(duty))
            
    def check_fault(self):
        '''
        @brief Checks the fault state of the fault detection routine.
        @details When the interrupt service routine is triggered by the activation
                 of the fault detection pin, nFAULT, this method disables all motor functions
                 to prevent hardware damage. If the fault remains active for a short period of time,
                 the user is required to input ENTER to reactivate motor functionality. If the fault detection
                 remains active after user input, this function will continue to disable all motor
                 functionality until the fault is cleared *and* the user presses ENTER.
        '''
        if self.pin_nFAULT.value() == 1:
            self.fault = False
            self.enable()
            print('Fault cleared.\n')
            
        else:
            input('''An extended fault has been detected. The motor driver will
                  remain disabled until the issue is fixed and the user presses ENTER.\n''')
            self.enable()
            while self.pin_nFAULT.value() == 0:
                input('The fault is still active. Check the issue again, then press ENTER.\n')
                self.disable()
                self.enable()
            
    def callback(self, IRQ):
        ''' 
        @brief   An external interupt callback function.
        @details An interrupt handler that is called when the nFAULT pin goes low.
        @param IRQ The desired physical interupt line.
        '''
        self.disable()
        self.fault = True
            
     

# The following block of code is an example of how to enable and drive both MotorDriver objects.     
if __name__ == '__main__':   
    print('What would you like to test?')
    print('\'1\' - Motor 1')
    print('\'2\' - Motor 2')
    print('\'M\' - Both Motors 1 & 2')
    print('\'F\' - Fault Detection for Both Motors')
    print('CTRL+C - Exit')
    
    # The testing mode of the example script: 1 == Motor 1, 2 == Motor 2, 'M' == Both Motors, 'F' == Fault Detection.
    ## The user-specified testing environment used to test/debug the motor driver.
    mode = input('\n')
    if mode == 'f' or mode == 'F':
        print('To simulate a fault, without potentially damaging the motors/driver chip, press the blue user button.')
        ## The state of whether to utilize fault detection.
        IRQ_mode = True
    else:
        IRQ_mode = False
        
        
    ## Motor nSLEEP pin object (used to enable the driver chip), initialized to PA5
    pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
    ## Motor nFAULT pin object (used for fault detection), initialized to the blue user button.
    pin_nFAULT = pyb.Pin(pyb.Pin.board.PB2, pyb.Pin.IN)
    ## A timer object using timer 3 with a frequency of 20kHz.
    tim3 = pyb.Timer(3, freq=20000)
    
    
    ## A dictionary item containing the pin address and channel number for the M1- output pin.
    M1_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B4),
              'Channel': 1 }
    ## A dictionary item containing the pin address and channel number for the M1+ output pin.
    M1_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B5),
              'Channel': 2 }
    ## A MotorDriver object used to control the output functions of Motor 1.
    moe1 = MotorDriver(pin_nSLEEP, pin_nFAULT, M1_CH1, M1_CH2, tim3, debug=False, IRQ=IRQ_mode)
    
    
    ## A dictionary item containing the pin address and channel number for the M2- output pin.
    M2_CH1 = {'Pin': pyb.Pin(pyb.Pin.cpu.B0),
              'Channel': 3 }
    ## A dictionary item containing the pin address and channel number for the M2+ output pin.
    M2_CH2 = {'Pin': pyb.Pin(pyb.Pin.cpu.B1),
              'Channel': 4 }
    ## A MotorDriver object used to control the output functions of Motor 2.
    moe2 = MotorDriver(pin_nSLEEP, pin_nFAULT, M2_CH1, M2_CH2, tim3, debug=False)
    
    # Enable BOTH motors.
    moe1.enable()
    moe2.enable()   
    
    try:
        # Run forever
        while True:
            try:
                # Test Motor 1
                if mode == '1':
                    # The user-specified duty cycle of the motor(s)
                    ## The PWM duty cycle applied to the motor.
                    duty = input('Please enter a duty cycle for Motor 1: ')
                    print()
                    moe1.set_duty(int(duty))
                
                # Test Motor 2
                elif mode == '2':
                    # The user-specified duty cycle of the motor(s)
                    duty = input('Please enter a duty cycle for Motor 2: ')
                    print()
                    moe2.set_duty(int(duty))  
                    
                # Test both motors in parallel
                elif mode == 'm' or mode == 'M':
                        duty = input('Please enter a duty cycle for both motors: ')
                        print()
                        moe1.set_duty(int(duty))
                        moe2.set_duty(int(duty))
    
                # test both motors for fault detection (Using the blue user button)
                elif mode == 'f' or mode == 'F':
                    # Set the motors to a duty cycle of -30 (lowest noise for my hardware)
                    moe1.set_duty(-35)
                    moe2.set_duty(-35)
    
                else:
                    mode = input('That was not a proper input. Please enter (M), (F), or (CTRL+C) to exit: ')
                    print()
                    pass
                pyb.delay(250)
                
            
            except:
                moe1.set_duty(0)
                moe2.set_duty(0)
                print('\n You pressed CTRL+C. Press \'1\', \'2\', \'M\', or \'F\', to start a new test;')
                mode = input('or press CTRL+C to exit the program entirely. \n\n')
                print()
                if mode == 'f' or mode == 'F':
                    print('\n To simulate a fault, without potentially damaging the motors/driver chip, press the blue user button. \n\n')
                    moe1 = MotorDriver(pin_nSLEEP, pin_nFAULT, M1_CH1, M1_CH2, tim3, IRQ=True)
                else:
                    moe1 = MotorDriver(pin_nSLEEP, pin_nFAULT, M1_CH1, M1_CH2, tim3, IRQ=False)
                moe1.enable()
            
    # Exit condition for errors or CTRL+C       
    except:
        moe1.disable()
        moe2.disable()
        print("Exiting Program")
    