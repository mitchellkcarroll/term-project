'''
@file  closedLoop.py

@brief  This file contains a closed-loop controller class.

@details  This class is used to calculate one iteration of closed-loop feeedback control.
The controller utilizes proportional, integral and derivative gain. To implement closed-loop control
appropriatle, the method update() should be called at regular intervals, along with updated encoder
velocities. Note: For best results, use a small value of Kd. 

@package Lab0x06
@package Lab0x07

@author Grant Gallagher

@date December 4, 2020
'''
class PID:
    '''
    @brief      Utilize PID control to calculate the necessary level output.
    
    @details    This class utilized PID control to apply accurate and responsive correction
                to a motor speed controller. The main function of this class, update(), runs one iteration
                to determine the error between the measured and actual speed of a motor, and return
                a level value that should be sent to the motor.
    '''
    def __init__(self, Kp, Ki, Kd):
        '''
        @brief Constructs a ClosedLoop object.
        @param Kp A positive number for proportional gain.
        @param Ki A positive number for integral gain.
        @param Kd A positive number for derivative gain.
        @param debug A boolean value that determines debugging state.
        @param interval The interval between updates, used to calculated Integral and Derivative error.
        '''
        ## The gain value for proportional control.
        self.Kp = Kp
        
        ## The gain value for integral control.
        self.Ki = Ki
        
        ## The gain value for derivative control.
        self.Kd = Kd
        
        ## The error between measured and desired motor speed.
        self.err = 0
        
        ## The motor speed error from the last iteration of ClosedLoop used for derivative control.
        self.err_prev = 0
        
        ## The proportional error, from Kp gain.
        self.P_err = 0
        
        ## The integral error, from Ki gain.
        self.I_err = 0
        
        ## The derivative error, from Kp gain.
        self.D_err = 0
        
        ## The level (%/rpm) sent to the motor driver.
        self.level = 0
        
        ## The previous level (%/rpm) sent to the motor driver.
        self.level_old = 0
        
        
    def update(self, W_ref, W_meas):
        '''
        @brief             Runs one iteration PID control the is used to determine motor level.
        @param W_ref       The reference (desired) motor speed.
        @param W_meas      The actual speed of the motor measured from an encoder.
        @return self.level The level, or duty cycle, that should be sent to the motor
                           to correct for the closed-loop error. 
        '''   
        
        self.err = (W_ref - W_meas) # Calculates the error      (proportional)
        
        
        self.P_err = self.Kp * self.err
        self.I_err += (self.Ki * self.err)# * (self.interval / 10**3)
        self.D_err = self.Kd * (self.err - self.err_prev)# / (self.interval / 10**3)
        
        self.level = self.P_err + self.I_err + self.D_err # + self.level_avg
        
        self.err_prev = self.err # Updates the previous error (derivative)
    
        return self.level # Returns the level