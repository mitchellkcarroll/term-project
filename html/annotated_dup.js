var annotated_dup =
[
    [ "bno055", null, [
      [ "BNO055", "classbno055_1_1BNO055.html", "classbno055_1_1BNO055" ]
    ] ],
    [ "bno055_base", null, [
      [ "BNO055_BASE", "classbno055__base_1_1BNO055__BASE.html", "classbno055__base_1_1BNO055__BASE" ]
    ] ],
    [ "closedLoop", null, [
      [ "ClosedLoop", "classclosedLoop_1_1ClosedLoop.html", "classclosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "encoderDriver", null, [
      [ "EncoderDriver", "classencoderDriver_1_1EncoderDriver.html", "classencoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "main0x03", null, [
      [ "UI_Back", "classmain0x03_1_1UI__Back.html", "classmain0x03_1_1UI__Back" ]
    ] ],
    [ "mcp9808", null, [
      [ "MCP9808", "classmcp9808_1_1MCP9808.html", "classmcp9808_1_1MCP9808" ]
    ] ],
    [ "motorDriver", null, [
      [ "MotorDriver", "classmotorDriver_1_1MotorDriver.html", "classmotorDriver_1_1MotorDriver" ]
    ] ],
    [ "pid", null, [
      [ "PID", "classpid_1_1PID.html", "classpid_1_1PID" ]
    ] ],
    [ "RTP_Driver", null, [
      [ "RTP_Driver", "classRTP__Driver_1_1RTP__Driver.html", "classRTP__Driver_1_1RTP__Driver" ]
    ] ],
    [ "RTP_Driver_Calibrated", null, [
      [ "RTP_Driver", "classRTP__Driver__Calibrated_1_1RTP__Driver.html", "classRTP__Driver__Calibrated_1_1RTP__Driver" ]
    ] ],
    [ "taskController", null, [
      [ "TaskController", "classtaskController_1_1TaskController.html", "classtaskController_1_1TaskController" ]
    ] ],
    [ "UI_Front", null, [
      [ "UI_Front", "classUI__Front_1_1UI__Front.html", "classUI__Front_1_1UI__Front" ]
    ] ],
    [ "vendotron", null, [
      [ "Vendotron", "classvendotron_1_1Vendotron.html", "classvendotron_1_1Vendotron" ]
    ] ]
];