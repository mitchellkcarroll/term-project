var Lab0x08_2encoderDriver_8py =
[
    [ "EncoderDriver", "classencoderDriver_1_1EncoderDriver.html", "classencoderDriver_1_1EncoderDriver" ],
    [ "E1_CH1", "Lab0x08_2encoderDriver_8py.html#a20eed3a25c32957b68aeca6bca8f28a2", null ],
    [ "E1_CH2", "Lab0x08_2encoderDriver_8py.html#a0f78f065951c548c54cbdb79c1d1ea2e", null ],
    [ "E2_CH1", "Lab0x08_2encoderDriver_8py.html#a982a1a5afe531a8f74b92426ec70c95c", null ],
    [ "E2_CH2", "Lab0x08_2encoderDriver_8py.html#a771470cd7368477cb495a459499eb55d", null ],
    [ "enc1", "Lab0x08_2encoderDriver_8py.html#ae74fdcbf87a28ca8b8d442e07d9f05a8", null ],
    [ "enc2", "Lab0x08_2encoderDriver_8py.html#a6b8aacb864b0349d0a2b47ba0e20e2ca", null ],
    [ "interval", "Lab0x08_2encoderDriver_8py.html#a249189d28d3f38a3c02699f16534e800", null ],
    [ "next_time", "Lab0x08_2encoderDriver_8py.html#aba9c32d2116642ebef33d1c6b4966eef", null ],
    [ "start_time", "Lab0x08_2encoderDriver_8py.html#a51d9e62d2412a7ab9fe8900a16b2ecf4", null ],
    [ "tim4", "Lab0x08_2encoderDriver_8py.html#a4168eaf668a428cb063b2b4936194e66", null ],
    [ "tim8", "Lab0x08_2encoderDriver_8py.html#a9a0c8a81be3af5fa423e7993dc64f62b", null ],
    [ "W_meas_1", "Lab0x08_2encoderDriver_8py.html#ada4a65f72a665bac9d5d64bc55487de3", null ],
    [ "W_meas_2", "Lab0x08_2encoderDriver_8py.html#aeaf7f5c0132c0e4a2164c6ab02acfa3d", null ]
];