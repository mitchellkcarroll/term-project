var ThinkFast_8py =
[
    [ "callback", "ThinkFast_8py.html#a84a93f6ecf7e7c347627bbd45601c58d", null ],
    [ "average_reaction_time", "ThinkFast_8py.html#aca9e46c371402f7a8b59a4a32e65ddda", null ],
    [ "button", "ThinkFast_8py.html#aecfedae66300b38c23703e39a5928048", null ],
    [ "button_pressed", "ThinkFast_8py.html#aa1021b0ec4db3b02ad775a293ba67a76", null ],
    [ "button_pressed_early", "ThinkFast_8py.html#a3bf69a31e41f30ed363892c1e5623291", null ],
    [ "delay_time", "ThinkFast_8py.html#a48803b66616c48a4e2a806680cc17c1c", null ],
    [ "extint", "ThinkFast_8py.html#a4ba8b69b724d46d26fae2e34d7ab4c81", null ],
    [ "LED", "ThinkFast_8py.html#a962d3827a6598d60aacef891fc60c2ed", null ],
    [ "reaction_list", "ThinkFast_8py.html#a27d3c2510ce0faee04b2a6f5b9a90c77", null ],
    [ "reaction_time", "ThinkFast_8py.html#ae38b9cec41fc1c0b3187d6c892e0258f", null ],
    [ "start_time", "ThinkFast_8py.html#a2b0cb83c579afdb308422009fbf43f16", null ],
    [ "timer", "ThinkFast_8py.html#ae884b3e284de64223366d5c0fbcc8898", null ]
];