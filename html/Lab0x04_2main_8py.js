var Lab0x04_2main_8py =
[
    [ "adc", "Lab0x04_2main_8py.html#afac357daa82cb1f4a2ca28df4f60bb14", null ],
    [ "amb_temp_c", "Lab0x04_2main_8py.html#aa9b3705f7b92550ab3df5e268320a071", null ],
    [ "amb_temp_f", "Lab0x04_2main_8py.html#a194176e26f234a52f609ef9b27597a76", null ],
    [ "core_temp", "Lab0x04_2main_8py.html#a6b57173838d77c174e4839adac2e39e8", null ],
    [ "current_time", "Lab0x04_2main_8py.html#a29cdf5a6de1e46e4afceb8f4a27460cc", null ],
    [ "debug", "Lab0x04_2main_8py.html#ad61adafb5044d42f7b8d262a172b446b", null ],
    [ "elapsed_time", "Lab0x04_2main_8py.html#ae6c4ef565c4a26bf6374430dde8709a3", null ],
    [ "end_time", "Lab0x04_2main_8py.html#abb59144f83fc840630482902fb047bd0", null ],
    [ "I2C_OBJECT", "Lab0x04_2main_8py.html#ae4ba93eb48235304cc2f8f2b8c8ca7ed", null ],
    [ "interval", "Lab0x04_2main_8py.html#a86060447ac22c1b649ef0497e8adf1ba", null ],
    [ "MCP9808_ADDRESS", "Lab0x04_2main_8py.html#a9155b23f0c7bf612fa19084dda445c60", null ],
    [ "next_time", "Lab0x04_2main_8py.html#a97359d1c36ca6a548999bed09f02f11a", null ],
    [ "sensor", "Lab0x04_2main_8py.html#a86cc0c2d3b40e8d33d8dd98b459621a2", null ],
    [ "start_time", "Lab0x04_2main_8py.html#ae57958345b17f9ca8597330ba07e1a1c", null ]
];