var main0x04_8py =
[
    [ "adc", "main0x04_8py.html#a34cee00fbde704dec088ad2919bc59e2", null ],
    [ "amb_temp_c", "main0x04_8py.html#afc2ca4f720d3f17081deecd2974b17b8", null ],
    [ "amb_temp_f", "main0x04_8py.html#a235e273c0290919a60cbfa788beafe21", null ],
    [ "core_temp", "main0x04_8py.html#a717be2c54d10d08be2a480d1b553cc3e", null ],
    [ "current_time", "main0x04_8py.html#a9c3a5f629c1a2c9855aa96fe7ed2dc10", null ],
    [ "debug", "main0x04_8py.html#a404e00b75b3d1c0daa8abc1ea7bacac2", null ],
    [ "elapsed_time", "main0x04_8py.html#a899ccf3ed36d370ae72d0f52b8bd296e", null ],
    [ "end_time", "main0x04_8py.html#af01e7f78f0312a3eb5c03ada3a78570f", null ],
    [ "I2C_OBJECT", "main0x04_8py.html#a38b2590800cc976044fb4167c6edd76c", null ],
    [ "interval", "main0x04_8py.html#a694dcb80ba6364218a1b67d729a0d6ed", null ],
    [ "MCP9808_ADDRESS", "main0x04_8py.html#a76591f4b15f1fc1807b9fb6c24c6b798", null ],
    [ "next_time", "main0x04_8py.html#a596c29a8e17db3a6b03e6c23b583e340", null ],
    [ "sensor", "main0x04_8py.html#ad7e638c484fd00fe0800641c11cb8082", null ],
    [ "start_time", "main0x04_8py.html#a3a652f44406882290d22b75c8791d419", null ]
];